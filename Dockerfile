FROM golang:1.20 as builder

WORKDIR /app
COPY . .
RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -o api .

FROM scratch
COPY --from=builder /app/api /usr/bin/api
CMD [ "api" ]