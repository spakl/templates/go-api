package main

import (
	"api/server"
	"api/utils"
	"github.com/sirupsen/logrus"
)
 
func main() {
	utils.ConfigureLogger()
	server := server.NewServer("3500")
	server.LoadMiddleware()
	
	errChan := make(chan error)
	
	go func (){
		err := server.Start()
		if err != nil {
			errChan <- err
		}

	}()
	go func() {
		if err := server.StartMetricsServer(); err != nil {
			errChan <- err
		}
	}()
	
	logrus.Fatal(<-errChan)
}

