package server

import (
	"api/utils"
	"github.com/gofiber/fiber/v2"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"

)

type Server struct {
	Port string
	App *fiber.App
}

func NewServer(port string) *Server{
	return &Server{
		Port: port,
		App: fiber.New(fiber.Config{
			Prefork: false,
			ServerHeader: "API",
		}),
	}
}

func (s *Server) LoadMiddleware(){
	s.App.Use(utils.LogrusContextMiddleware)
	s.App.Use(utils.OtelMiddleware)
}


func (s *Server) Start()error{
	s.App.Get("/tracers/:id", GetTracer )
	s.App.Get("/test", SomeHandler)
	return s.App.Listen(":" + s.Port )
}

func (s *Server) StartMetricsServer() error {
	promHandler := fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler())
	s.App.Get("/metrics", func(c *fiber.Ctx) error {
		promHandler(c.Context())
		return nil
	})
	return s.App.Listen(":8090")
}