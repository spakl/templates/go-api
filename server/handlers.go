package server

import (
	"api/utils"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	oteltrace "go.opentelemetry.io/otel/trace"
)


func SomeHandler(c *fiber.Ctx) error {
	logger := c.Locals("logger").(*logrus.Entry)  // Type assertion to retrieve the logger instance
	logger.Info("Logging Test with Context Logger")
	logger.Debug("Debug message")
	tracer := c.Locals("tracer").(oteltrace.Tracer) 
	_, span := tracer.Start(c.Context(), "test")
	defer span.End()

	// ... other logic ...
	return c.Status(fiber.StatusOK).JSON(&fiber.Map{
		"msg":"success!",
	})
}

func GetTracer(c *fiber.Ctx) error {
	logger := c.Locals("logger").(*logrus.Entry)  // Type assertion to retrieve the logger instance
	logger.Info("Getting Tracer")
	id := c.Params("id")
	name := utils.GetTrace(c, id)
	return c.Status(fiber.StatusOK).JSON(fiber.Map{"id": id, name: name})
}

