package utils

import (
	"context"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"

	"github.com/gofiber/contrib/otelfiber"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	stdout "go.opentelemetry.io/otel/exporters/stdout/stdouttrace"

	//"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	oteltrace "go.opentelemetry.io/otel/trace"
)


func OtelMiddleware(c *fiber.Ctx) error {
	tp := initTracer()
	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			logrus.Error("error shutting down tracer provider", err)
		}
	}()

	c.Locals("tracer", otel.Tracer("api"))
	c.App().Use(otelfiber.Middleware())
	return  c.Next()
}

func initTracer() *sdktrace.TracerProvider {
	exporter, err := stdout.New(stdout.WithPrettyPrint())
	if err != nil {
		logrus.Fatal(err)
	}
	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exporter,sdktrace.WithBatchTimeout(100 * time.Millisecond)),
		sdktrace.WithResource(
			resource.NewWithAttributes(
				semconv.SchemaURL,
				semconv.ServiceNameKey.String("api"),
			)),
	)
	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))
	return tp
}
var Tracer = otel.Tracer("api")

func GetTrace(ctx *fiber.Ctx, id string) string {
	_, span := CreateSpanWithAttr(ctx, "GetTrace","id",id)
	defer span.End()
	if id == "123" {
		return "otelfiber tester"
	}
	return "unknown"
}

func CreateSpan(ctx *fiber.Ctx, spanName string) (context.Context, oteltrace.Span) {
	context, span := Tracer.Start(ctx.UserContext(), spanName)
	return context, span
}

func CreateSpanWithAttr(ctx *fiber.Ctx, spanName, key, value string) (context.Context, oteltrace.Span) {
	context, span := Tracer.Start(ctx.UserContext(), spanName, oteltrace.WithAttributes(attribute.String(key, value)))
	return context, span
}