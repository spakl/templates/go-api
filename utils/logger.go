package utils

import (
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)
func ConfigureLogger() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})	
	logrus.SetReportCaller(true)
}
func LogrusMiddleware(c *fiber.Ctx) error {
	logger := logrus.WithFields(logrus.Fields{
		"method": c.Method(),
		"path":   c.Path(),
		"ip":     c.IP(),
	})


	// Pass to the next handler function
	err := c.Next()
	if err != nil {
		logger.WithError(err).Error("Request error")
		return err
	}
	return nil
}

func LogrusContextMiddleware(c *fiber.Ctx) error {
	// Create a logger instance with some default fields
	entry := logrus.WithFields(logrus.Fields{
		// "request_id": c.ID(),  // Assuming you're generating a unique ID for each request
		"method":     c.Method(),
		"path":       c.Path(),
		"ip":         c.IP(),
	})

	// Store the logger instance in the context's Locals
	c.Locals("logger", entry)

	// Pass to the next handler function
	return c.Next()
}